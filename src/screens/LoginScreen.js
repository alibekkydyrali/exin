import React, { Component } from 'react';
import { SafeAreaView, View, TouchableOpacity, Text, AsyncStorage, TextInput, Dimensions, TouchableWithoutFeedback, Keyboard, Image } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import axios from 'axios';
import { navigateTo, navigatePop } from '../actions';
import { Button } from '../components/common/Button';
const { width } = Dimensions.get('window');
import FCM, { NotificationActionType } from "react-native-fcm";

class LoginScreen extends Component {
  onNavigateTo(path) {
    this.props.navigateTo(path);
  }

  onNavigatePop() {
    this.props.navigatePop();
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'Admin');
    this.onNavigateTo('AuthDrawer');
  };

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
    }
  }

  async componentDidMount() {
    FCM.getFCMToken().then(token => {
      this.setState({ id: token })
      // console.warn(this.state.id)
    });
  }

  LogIn = async () => {
    fetch('http://cb.ex-group.kz:8080/ExclusiveStroy/Client', {
      method: 'POST',

      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'type': 'authentication',
        'POINT_ID': '0',
        'login': this.state.login,
        'password': this.state.password,
        'session': '0',
        'client': 'exstroy',
        'format': 'json'

      }),
    }).then((response) => {
      response.json().then((responseJson) => {
        if (responseJson.AUTHENTICATION == 1 && responseJson.USERUUID != null && responseJson.EMPLOYEE_UUID != null && responseJson.USERID != null) {
          AsyncStorage.setItem('user', JSON.stringify(responseJson));
          AsyncStorage.setItem('id', JSON.stringify(this.state.id));
          this.props.navigateTo('AuthDrawer');
        }
      });

    })

      .catch((error) => {
        console.error(error);
      });

  }


  render() {
    const {
      containerStyle,
      headerStyle,
      passButton,
      passButtonTitle,
      marginContainerStyle,
      titleStyle,
      bodyStyle,
    } = styles;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <SafeAreaView style={containerStyle}>

          <View style={marginContainerStyle}>
            <Image source={require('./images/logo72x72.png')} />
            <Text style={titleStyle}>Добро пожаловать!</Text>
          </View>

          <View style={bodyStyle}>

            <TextInput
              style={{
                width: 300,
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 20,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 5,
                marginBottom: 5,
                paddingLeft: 15
              }}
              onChangeText={(text) => this.setState({ login: text })}
              placeholder="Логин"
              value={this.state.text}
              autoCorrect={false}
            />
            <TextInput
              style={{
                width: 300,
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 20,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 5,
                marginBottom: 5,
                paddingLeft: 15
              }}
              onChangeText={(text) => this.setState({ password: text })}
              placeholder="Пароль"
              value={this.state.text}
              secureTextEntry={true}
            />

            <Button
              onPress={this.LogIn}
              title={'Войти'}
            />

          </View>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    );
  }
}

LoginScreen.propTypes = {
  navigateTo: PropTypes.func,
  navigatePop: PropTypes.func,
};

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#004d40',
  },
  headerStyle: {
    flex: 0.1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  passButton: {
    width: 160,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 20,
  },
  passButtonTitle: {
    color: 'gray',
    fontSize: 15,
  },
  marginContainerStyle: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleStyle: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold',
  },
  bodyStyle: {
    flex: 0.4,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default connect(null, { navigateTo, navigatePop })(LoginScreen);
