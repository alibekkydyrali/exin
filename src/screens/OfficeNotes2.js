import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator, AsyncStorage, SafeAreaView } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import PropTypes from 'prop-types';

class OfficeNotes2
 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      user: '',
      object_name: '',
      status: false,
      notification: '',
    };
  }

  async componentDidMount() {
    await AsyncStorage.getItem('user').then(value => {
      var parsed = JSON.parse(value);
      this.setState({ user: parsed });
      // console.warn('JSON:', parsed);
    })
      .catch((error) => {
        console.error(error);
      });
    this.makeRemoteRequest();
    this.notificationRequest();
  };

  makeRemoteRequest = () => {
    const url = 'http://cb.ex-group.kz:8080/EX-IN-SERVLET/service';
    this.setState({ loading: true });

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'DATATYPE': 'list',
        'ACTION': 'getWorknotes',
        'STATUS': 'recipient',
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
      })
    }).then(res => res.json())
      .then(res => {
        this.setState({
          data: res,
          error: res.error || null,
          loading: false,
          refreshing: false
        });
        //  console.warn('otvet:',res.data);
        // AsyncStorage.setItem('note', JSON.stringify(res));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  notificationRequest = () => {
    this.setState({ loading: true });
    fetch('http://cb.ex-group.kz:8080/EX-IN-SERVLET/service', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'USER_ID': this.state.user.USERID,
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'USER_UUID': this.state.user.USERUUID,
        'ACTION': 'getNotificationsByUser',
      })
    }).then(res => res.json())
      .then(res => {
        // console.warn(res);
        this.setState({
          notification: res.data,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
      });
  }


  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
        this.notificationRequest();
      }
    );
  };


  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar placeholder="Поиск..." lightTheme round />;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
      <View containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <FlatList
          data={this.state.data.data}
          keyExtractor={item => item.worknotes_id}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          renderItem={(message) => {
            const item = message.item;
            const a = [];

            if (this.state.notification != 0) {
              this.state.notification.map((object) => {
                a.push(object.instance_uuid);
              });
            }
            let read = a.includes(item.worknotes_uuid);
            let itemStyle = read ? styles.read : styles.unread;

            return (
              <ListItem
                title={'№: ' + item.worknotes_docnumber + ', ' + 'Дата: ' + item.worknotes_docdate}
                subtitle={item.worknotes_subject}
                subtitleNumberOfLines={2}
                containerStyle={itemStyle}
                onPress={() => { this.props.navigation.navigate('Note', { item }) }}
              />
            )
          }}
        />
      </View>


    );
  }
}

OfficeNotes2.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  read: {
    backgroundColor: 'pink',
    borderBottomWidth: 0,
    // paddingTop:50
  },
  unread: {
    backgroundColor: 'white',
    borderBottomWidth: 0,
    // paddingBottom:50
  },
};



export default OfficeNotes2;
