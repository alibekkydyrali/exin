import React, { Component } from 'react';
import { AsyncStorage, SafeAreaView, Text } from 'react-native';
import { Button } from 'native-base';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { navigateTo, navigatePop } from '../actions';
import { View } from 'native-base';

class LogoutScreen extends Component {
  static navigationOptions = {
    title: 'Выход',
  };

  _onPressLogout = async () => {
    await AsyncStorage.clear();
    this.props.navigateTo('UnauthModal');
  };

  render() {
    return (
      <SafeAreaView style={styles.containerStyle}>
        <Text style={{ fontSize: 20 }}>Выйти?</Text>
        <View style={{ flexDirection: 'row' }}>
          <Button warning rounded onPress={() => this._onPressLogout()} style={{
            width: 70,
            height: 40,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          >
            <Text>Да</Text>
          </Button>
          <Button warning rounded onPress={() => { this.props.navigatePop(); }} style={{
            width: 70,
            height: 40,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            <Text>Нет</Text>
          </Button>
        </View>
      </SafeAreaView>
    );
  }
}

LogoutScreen.propTypes = {
  navigateTo: PropTypes.func,
  navigatePop: PropTypes.func,
};

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default connect(null, { navigateTo, navigatePop })(LogoutScreen);
