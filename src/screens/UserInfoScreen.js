import React, { Component } from 'react';
import { AsyncStorage, SafeAreaView, Text, Button } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { navigateTo } from '../actions';

class UserInfoScreen extends Component {
  static navigationOptions = {
    title: 'Профиль',
  };

  state = {
    user: '',
  };

  componentDidMount() {
    AsyncStorage.getItem('user').then(value => {
      this.setState({ user: value });
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.containerStyle}>
        <Text>Ваш ID : {this.state.user}</Text>
        <Button
          title={'Выйти'}
          onPress={() => {
            this.props.navigateTo('Logout');
          }}
        />
      </SafeAreaView>
    );
  }
}

UserInfoScreen.propTypes = {
  navigation: PropTypes.object,
  navigateTo: PropTypes.func,
};

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default connect(null, { navigateTo })(UserInfoScreen);
