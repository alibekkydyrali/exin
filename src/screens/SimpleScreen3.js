import React, { Component } from 'react';
import { SafeAreaView, Text } from 'react-native';
import PropTypes from 'prop-types';

import { Button } from '../components/common/Button';

class SimpleScreen3 extends Component {
  static navigationOptions = {
    title: 'Задачи на контроле',
  };

  render() {
    return (
      <SafeAreaView style={styles.containerStyle}>
        <Text>Страница на разработке</Text>
        
      </SafeAreaView>
    );
  }
}

SimpleScreen3.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default SimpleScreen3;
