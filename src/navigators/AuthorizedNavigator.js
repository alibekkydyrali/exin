import { DrawerNavigator } from 'react-navigation';

import NoteModalNavigator from './NoteModalNavigator';
import LogoutScreen from '../screens/LogoutScreen';
import NoteScreen from '../screens/NoteScreen'
import SimpleScreen2 from '../screens/SimpleScreen2'
import SimpleScreen3 from '../screens/SimpleScreen3'
import SimpleScreen4 from '../screens/SimpleScreen4'
import SimpleScreen5 from '../screens/SimpleScreen5'

const routeConfig = {
  NoteNav: {screen: NoteModalNavigator,navigationOptions: { title: 'Главная страница' }},
  Tasks: {screen: SimpleScreen2,navigationOptions: { title: 'Мои задачи' }},
  TasksOnControl: {screen: SimpleScreen3,navigationOptions: { title: 'Задачи на контроле' }},
  Worknotes: {screen: SimpleScreen4,navigationOptions: { title: 'Служебные записки' }},
  News: {screen: SimpleScreen5,navigationOptions: { title: 'Новости' }},
  Logout: {screen:LogoutScreen, navigationOptions: { title: 'Выйти'}}
};

const navigatorConfig = {
  contentOptions: {
    activeTintColor: 'white',
    activeBackgroundColor: '#004d40',
  },
};

const AuthorizedNavigator = DrawerNavigator(routeConfig, navigatorConfig);

export default AuthorizedNavigator;
