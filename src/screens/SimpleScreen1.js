import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Text, Left, Right, Icon, Badge, Row, } from 'native-base';
import { View, SafeAreaView, TouchableOpacity, YellowBox, AsyncStorage, RefreshControl } from 'react-native';
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Button } from '../components/common/Button';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

class SimpleScreen1 extends Component {
  static navigationOptions = {
    title: 'Список',
  };
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      user: '',
      count: '',
      data: '',
      refreshing: false,
      token: "",
      tokenCopyFeedback: ""
    };
  }
  notificationRequest = () => {
    this.setState({ loading: true });
    fetch('http://cb.ex-group.kz:8080/EX-IN-SERVLET/service', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'USER_ID': this.state.user.USERID,
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'USER_UUID': this.state.user.USERUUID,
        'ACTION': 'getNotificationsByUser',
      })
    }).then(res => res.json())
      .then(res => {
        this.setState({ data: res });
        // console.warn('getnotification:', { data: this.state.data.data});
        b = [];
        if (res.data != 0) {
          res.data.map((object) => {
            b.push(object.object_name);
            this.setState({ count: b })
          });

        }

        this.setState({
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  async componentDidMount() {
    await AsyncStorage.getItem('user').then(value => {
      var parsed = JSON.parse(value);
      this.setState({ user: parsed });
      // console.warn('JSON:', parsed);
    })
      .catch((error) => {
        console.error(error);
      });
    this.notificationRequest();    
  }


  

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => {
        this.notificationRequest();
      }
    );
  };


  render() {
    // return (
    //   <Container >
    //     <Content
    //       refreshControl={
    //         <RefreshControl
    //           onRefresh={this.handleRefresh}
    //           refreshing={this.state.refreshing}
    //         />
    //       }>
    //       <List>
    //         <ListItem
    //           title={'Мои задачи'}
    //           onPress={() => { this.props.navigation.push('Simple2'); }}
    //         >
    //           <Left>
    //             <Text>Мои задачи</Text>
    //           </Left>
    //           <Right>
    //             <Icon name="arrow-forward" />
    //           </Right>
    //         </ListItem>
    //         <ListItem
    //           title={'Задачи на контроле'}
    //           onPress={() => { this.props.navigation.navigate('Simple3'); }}
    //         >
    //           <Left>
    //             <Text>Задачи на контроле</Text>
    //           </Left>
    //           <Right>
    //             <Icon name="arrow-forward" />
    //           </Right>
    //         </ListItem>
    //         <ListItem
    //           title={'Служебные записки'}
    //           onPress={() => { this.props.navigation.navigate('Simple4'); }}
    //         >
    //           <Left>
    //             <Text>Служебные записки</Text>
    //           </Left>
    //           <Right>
    //             <Badge warning >
    //               <Text>{this.state.count.length}</Text>
    //             </Badge>
    //           </Right>
    //         </ListItem>
    //         <ListItem
    //           title={'Новости'}
    //           onPress={() => { this.props.navigation.navigate('Simple5'); }}
    //         >
    //           <Left>
    //             <Text>Новости</Text>
    //           </Left>
    //           <Right>
    //             <Icon name="arrow-forward" />
    //           </Right>
    //         </ListItem>
    //       </List>



    //     </Content>
    //     <Button
    //       title={'Вернуться назад'}
    //       onPress={() => {
    //         this.props.navigation.goBack(null);
    //       }}
    //       style={styles.containerStyle}
    //     />
    //   </Container>
    // );
  }
}
// SimpleScreen1.propTypes = {
//   navigation: PropTypes.object,
// };

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default SimpleScreen1;