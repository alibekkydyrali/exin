import { StackNavigator } from 'react-navigation';
import NoteScreen from '../screens/NoteScreen'
import NoteStack from './NoteStackNavigator';
import NoteDetail from '../screens/NoteDetailScreen';
import SimpleStack from './SimpleStackNavigator';

const routeConfig = {
  NoteScreen: {
    screen: NoteScreen,
    navigationOptions: {
      // header: null,
    },
  },
  NoteModal: {
    screen: NoteDetail,
  },
  SimpleStack: {
    screen: SimpleStack,
    navigationOptions: {
      header: null,
    },
  },
};

const navigationConfig = {
  mode: 'modal',
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#004d40',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
};

const NoteModalNavigator = StackNavigator(routeConfig, navigationConfig);

export default NoteModalNavigator;
