import { StackNavigator, DrawerNavigator } from 'react-navigation';

import NoteScreen from '../screens/NoteScreen';
import OfficeNotes1 from '../screens/OfficeNotes1';
import OfficeNotes2 from '../screens/OfficeNotes2';
import SimpleScreen2 from '../screens/SimpleScreen2';
import SimpleScreen3 from '../screens/SimpleScreen3';
import SimpleScreen4 from '../screens/SimpleScreen4';
import SimpleScreen5 from '../screens/SimpleScreen5';
import CreateNote from '../screens/CreateNote';
import NoteDetail from '../screens/NoteDetail';

const routeConfig = {
  NoteScreen: NoteScreen,
  OfficeNotes1: OfficeNotes1,
  OfficeNotes2: OfficeNotes2, 
  Simple2: SimpleScreen2,
  Simple3: SimpleScreen3,
  Simple4: SimpleScreen4,
  Simple5: SimpleScreen5,
  CreateNote: CreateNote,
  Note: NoteDetail,
};

const navigationConfig = {
  initialRouteName: 'NoteScreen',
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#004d40',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
};

const SimpleStackNavigator = StackNavigator(routeConfig, navigationConfig);

export default SimpleStackNavigator;
