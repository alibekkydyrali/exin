import React, { Component } from 'react';
import { SafeAreaView, StatusBar, View, Text } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { navigateTo } from '../actions';
import { Button } from '../components/common/Button';

class LandingScreen extends Component {
  onNavigateTo(path) {
    this.props.navigateTo(path);
  }

  render() {
    const { containerStyle, marginContainerStyle, titleStyle, bodyStyle } = styles;

    return (
      <SafeAreaView style={containerStyle}>
        <StatusBar hidden={true} />

        <View style={marginContainerStyle}>
          <Text style={titleStyle}>Добро пожаловать!</Text>
        </View>

        <View style={bodyStyle}>
          <Button
            onPress={() => {
              this.onNavigateTo('Login');
            }}
            title={'Войти'}
          />
        </View>
      </SafeAreaView>
    );
  }
}

LandingScreen.propTypes = {
  navigation: PropTypes.object,
  navigateTo: PropTypes.func,
};

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#004d40',
  },
  marginContainerStyle: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleStyle: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold',
  },
  bodyStyle: {
    flex: 0.4,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default connect(null, { navigateTo })(LandingScreen);
