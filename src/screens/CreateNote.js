import React, { Component } from 'react';
import { TouchableWithoutFeedback, View, Text, TouchableOpacity, Keyboard, AsyncStorage, Animated, ScrollView, Platform, TextInput, Button } from 'react-native';
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ModalFilterPicker from 'react-native-modal-filter-picker';
// import { Button } from '../components/common/Button';
const x = [];
class CreateNote extends Component {


  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Создание служебной записки',
      headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.navigate('Simple4')}
        >
          <Ionicons
            name={'md-arrow-back'}
            size={25}
            color={'white'}
            style={{ marginLeft: 15 }}
          />
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      recipients: [],
      data: [],
      data2: [],
      valueArray: [],
      disabled: false,
      subject: '',
      description: '',
    };

    this.index = 0;
    this.addNewEle = false;
    this.animatedValue = new Animated.Value(0);

  }
  async componentDidMount() {

    await AsyncStorage.getItem('user').then(value => {
      var parsed = JSON.parse(value);
      this.setState({ user: parsed });
      // console.warn('JSON:', parsed);
    })
      .catch((error) => {
        console.error(error);
      });
    this.getPositionsSelectOptions();
  };

  getPositionsSelectOptions = () => {
    fetch('http://cb.ex-group.kz:8080/ExclusiveServlet/service', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'USER_ID': this.state.user.USERID,
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'TYPE': 'CATALOG',
        'DATATYPE': 'list',
        'ACTION': 'getPositionsSelectOptions'
      })
    }).then(res => res.json())
      .then(res => {
        // console.warn(res);
        var a = {
          b: []
        };


        for (var i in res) {

          var item = res[i];
          if (item.key && item.label != null)
            a.b.push({
              "key": item.key,
              "label": item.label
            });
          x[item.key] = {
            "individname": item.individ_name,
            "positionname": item.position_name,
            "employeeuuid": item.employee_uuid,
            "individuuid": item.individ_uuid,
            "chatid": item.chat_id,
            "positionuuid": item.position_uuid,
            "useruuid": item.user_uuid,
          }
          // console.warn(a)
        }
        this.setState({ data: a.b });
      })
      .catch(error => {
        console.error(error);
      });
  }



  createNote = () => {
    if(this.state.recipients.length != 0 && this.state.user.USERID != null && this.state.user.EMPLOYEE_UUID != null && this.state.subject !='' && this.state.description !=''){
      alert('Служебная записка создана');
      this.props.navigation.navigate('Simple4')
      fetch('http://cb.ex-group.kz:8080/EX-IN-SERVLET/service', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'USER_ID': this.state.user.USERID,
          'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
          'EMPLOYEE_NAME': this.state.user.EMPLOYEE_NAME,
          'TYPE': 'CATALOG',
          'DATATYPE': 'instance',
          'ACTION': 'insertWorknote',
          "DATA": {
            "subject": this.state.subject,
            'description': this.state.description,
            "recipients": this.state.recipients
          }
        })
      }).then(res => res.json())
        .then(res => {
          // console.warn(res);
  
          var a = {
            b: []
          };
  
          for (var i in res) {
  
            var item = res[i];
            if (item.key && item.label != null)
              a.b.push({
                "key": item.key,
                "label": item.label,
              });
            // console.warn(a)
          }
          this.setState({ data: a.b });
        })
        .catch(error => {
          console.error(error);
        });
    }
    if(this.state.recipients.length==0){
      alert('Выберите получателя');
    }
    if (this.state.subject==''){
      alert('Заполните поле "Тема"');
    }
    if (this.state.description==''){
      alert('Заполните поле "Описание"');
    }
  }


  onSelect = (picked) => {
    var recipients = x[picked];
    recipients["key"] = picked;
    // console.warn(recipients)
    this.state.recipients.push(recipients);
    this.setState({ visible: false });
    this.state.recipients.map((object) => {
      // console.warn(object.useruuid);
    })
  }

  onCancel = () => {
    this.setState({
      visible: false
    });
  }

  addRow = () => {
    this.setState({ visible: true });

    let newlyAddedValue = { index: this.index }

    this.setState({ disabled: true, valueArray: [...this.state.valueArray, newlyAddedValue] }, () => {
      Animated.timing(
        this.animatedValue,
        {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }
      ).start(() => {
        this.index = this.index + 1;
        this.setState({ disabled: false });
      });
    });
  }

  remove(id) {
    const newArray = this.state.recipients;
    newArray.splice(id, 1);
    // console.warn(newArray);
    this.setState(() => {
      return {
        recipients: newArray
      }
    }
    );
  }



  render() {
    const { visible, recipients } = this.state;

    const animationValue = this.animatedValue.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-59, 0]
      });

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.container}>
        <ModalFilterPicker
            visible={visible}
            onSelect={this.onSelect}
            onCancel={this.onCancel}
            options={this.state.data}
            placeholderText='Фильтер'
            cancelButtonText='Отмена'
            noResultsText='Не найдено'
          />
         
            <View style={{marginTop:0}}>
            <TouchableOpacity
              onPress={this.addRow}
              title="Добавить получателя +"
              disabled={this.state.disabled}
              style={{
                width: '100%',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 20,
                // marginLeft: 30,
                // marginRight: 30,
                marginTop: 5,
                marginBottom: 5,
              }}
            ><Text style={{
              color: '#004d40',
              fontSize: 20,
              fontWeight: 'bold',
            }}>Добавить получателя +</Text>
            </TouchableOpacity>
            </View>
            <ScrollView>
            {this.state.recipients.map((object, i) => {
              return (<View key={i} style={{ flexDirection: 'row', marginLeft: 15 }}>
                <TouchableOpacity onPress={this.addRow} style={{ width: '85%',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 10,
                // marginLeft: 30,
                // marginRight: 30,
                marginTop: 5,
                marginBottom: 5,}}>
                  <Text>{object.key}</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.remove(i)} disabled={this.state.disabled} style={{ width: 40,
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 10,
                marginLeft: 10,
                // marginRight: 30,
                marginTop: 5,
                marginBottom: 5, }}>
                  <Text>x</Text>
                </TouchableOpacity>

              </View>
              )
              

            })}

            <TextInput
              style={{
                width: '93%',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 10,
                marginLeft: 15,
                marginRight: 15,
                marginTop: 5,
                marginBottom: 5,
                paddingLeft: 15,
              }}
              onChangeText={(text) => this.setState({ subject: text })}
              placeholder="Тема"
              value={this.state.text}
              autoCorrect={false}
            />
            <TextInput
              style={{
                width: '93%',
                height: 200,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 10,
                marginLeft: 15,
                marginRight: 15,
                marginTop: 5,
                marginBottom: 5,
                paddingLeft: 15
              }}
              onChangeText={(text) => this.setState({ description: text })}
              placeholder="Описание"
              value={this.state.text}
              autoCorrect={false}
              multiline={true}
            />
            </ScrollView>


            <View style={{bottom:0}}>
            <TouchableOpacity
              onPress={this.createNote}
              title="Создать"
              style={{
                width: '100%',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#fff',
                borderRadius: 20,
                // marginLeft: 30,
                // marginRight: 30,
                marginTop: 5,
                marginBottom: 5,
              }}
            >
            <Text style={{
              color: '#004d40',
              fontSize: 20,
              fontWeight: 'bold',
            }}>Создать служебную записку
            </Text>
            </TouchableOpacity>
            </View>
         
        </View>

      </TouchableWithoutFeedback>
    );
  }
}

CreateNote.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  container:
  {
    backgroundColor: '#eee',
    flex: 1,
    paddingTop: (Platform.OS == 'ios') ? 20 : 0
  },

  viewHolder:
  {
    height: 55,
    backgroundColor: '#26A69A',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 4
  },

  text:
  {
    color: 'white',
    fontSize: 25
  },

  btn:
  {
    position: 'absolute',
    right: 25,
    bottom: 25,
    borderRadius: 30,
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.7)',
    padding: 15
  },

  btnImage:
  {
    resizeMode: 'contain',
    width: '100%',
    tintColor: 'white'
  }
};

export default CreateNote;
