import React, { Component } from 'react';
import {
  Text,
  AsyncStorage,
  View,
  Animated,
  TouchableHighlight,
  Image,
  ScrollView,
  FlatList,
  TouchableOpacity,
  TextInput,
  Keyboard
} from 'react-native';
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Button } from '../components/common/Button';
import Modal from "react-native-modal";
import ImagePicker from 'react-native-image-picker';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';




class NoteDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Служебные записки',
      headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.navigate('Simple4')}
        >
          <Ionicons
            name={'md-arrow-back'}
            size={25}
            color={'white'}
            style={{ marginLeft: 15 }}
          />
        </TouchableOpacity>
      ),

    };
  };

  constructor(props) {
    super(props);

    this.icons = {     //Step 2
      'up': require('./images/Arrowhead-01-128.png'),
      'down': require('./images/Arrowhead-Down-01-128.png')
    };

    this.state = {
      note: [],
      recipients: '',
      user: '',
      worknote: [],
      activeSections: [],
      title: 'Описание:',
      expanded: true,
      animation: new Animated.Value(),
      visibleModal: null,
      avatarSource: null,
      videoSource: null,
      fileUri: '',
      fileType: '',
      fileName: '',
      fileSize: '',
      message: '',

    };
    this.selectPhoto = this.selectPhoto.bind(this);
    this.selectVideo = this.selectVideo.bind(this);
  }

  clear = () => {
    this.textInputRef.clear();
  }

  sendMessage = () => {
    let formdata = new FormData();
    if(this.props.navigation.state.params.notif==undefined){
      
      formdata.append("worknoteUUID", this.props.navigation.state.params.item.worknotes_uuid)
      formdata.append("employeeName", this.state.user.EMPLOYEE_NAME)
      formdata.append("employeeUUID", this.state.user.EMPLOYEE_UUID)
      formdata.append("userId", this.state.user.USERID)
      formdata.append("userUUID", this.state.user.USERUUID)
      formdata.append("worknoteText", this.state.message)
      formdata.append("isMobile", true)
      formdata.append("countScan", '0')
    // console.warn('message:', this.state.message);
    }
    else{
      formdata.append("worknoteUUID", this.props.navigation.state.params.notif.notification.instance_uuid)
      formdata.append("employeeName", this.state.user.EMPLOYEE_NAME)
      formdata.append("employeeUUID", this.state.user.EMPLOYEE_UUID)
      formdata.append("userId", this.state.user.USERID)
      formdata.append("userUUID", this.state.user.USERUUID)
      formdata.append("worknoteText", this.state.message)
      formdata.append("isMobile", true)
      formdata.append("countScan", '0')
    }
    fetch('http://cb.ex-group.kz:8080/ex-in-uploader/service', {
      method: 'POST',
      headers: {
        "Content-Type": "multipart/form-data",
        "Accept": "application/json",
        "type": "formData",
      },
      body: formdata
    }).then(response => {
      // console.warn("message", response)
      this.makeRemoteDetailRequest();
      this.makeRemoteNoteRequest();
      this.deleteNotification();
      this.setState({ message: '' });
    }).catch(err => {
      console.warn(err)
    }).then(Keyboard.dismiss).then(this.textInput.clear())

  }

  selectPhoto() {
    const options = {
      title: 'Выберите фотографию',
      takePhotoButtonTitle: 'Открыть камеру',
      cancelButtonTitle: 'Отмена',
      chooseFromLibraryButtonTitle: 'Выбрать с альбома',
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.warn('Response = ', response);

      if (response.didCancel) {
        console.warn('User cancelled photo picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };
        let formdata = new FormData();
        formdata.append("worknoteUUID", this.props.navigation.state.params.item.worknotes_uuid)
        formdata.append("employeeName", this.state.user.EMPLOYEE_NAME)
        formdata.append("employeeUUID", this.state.user.EMPLOYEE_UUID)
        formdata.append("userId", this.state.user.USERID)
        formdata.append("userUUID", this.state.user.USERUUID)
        formdata.append("worknoteText", '')
        formdata.append("countScan", '1')
        formdata.append("worknoteScan0", { uri: source.uri, name: 'imageFromMobile.jpg', type: 'multipart/form-data' })

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        console.warn('formdata:', formdata);

        fetch('http://cb.ex-group.kz:8080/ex-in-uploader/service', {
          method: 'post',
          headers: {
            "Content-Type": "multipart/form-data",
            "Accept": "application/json",
            "type": "formData"
          },
          body: formdata
        }).then(response => {
          console.log(response)
        }).catch(err => {
          console.log(err)
        });
      }
    });
  }

  selectVideo() {
    const options = {
      title: 'Выберите видео',
      takePhotoButtonTitle: 'Открыть камеру',
      cancelButtonTitle: 'Отмена',
      chooseFromLibraryButtonTitle: 'Выбрать с альбома',
      mediaType: 'video',
      videoQuality: 'medium',
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.warn('Response = ', response);

      if (response.didCancel) {
        console.warn('User cancelled video picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };
        let formdata = new FormData();
        formdata.append("worknoteUUID", this.props.navigation.state.params.item.worknotes_uuid)
        formdata.append("employeeName", this.state.user.EMPLOYEE_NAME)
        formdata.append("employeeUUID", this.state.user.EMPLOYEE_UUID)
        formdata.append("userId", this.state.user.USERID)
        formdata.append("userUUID", this.state.user.USERUUID)
        formdata.append("worknoteText", '')
        formdata.append("countScan", '1')
        formdata.append("worknoteScan0", { uri: source.uri, name: 'videoFromMobile', type: 'multipart/form-data' })

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        console.warn('formdata:', formdata);

        fetch('http://cb.ex-group.kz:8080/ex-in-uploader/service', {
          method: 'post',
          headers: {
            "Content-Type": "multipart/form-data",
            "Accept": "application/json",
            "type": "formData"
          },
          body: formdata
        }).then(response => {
          console.log(response)
        }).catch(err => {
          console.log(err)
        });
      }
    });
  }


  selectFile() {
    //Opening Document Picker
    DocumentPicker.show(
      {
        filetype: [DocumentPickerUtil.allFiles()],
        //All type of Files DocumentPickerUtil.allFiles()
        //Only PDF DocumentPickerUtil.pdf()
        //Audio DocumentPickerUtil.audio()
        //Plain Text DocumentPickerUtil.plainText()
      },
      (error, res) => {
        if (res == null) {
          console.warn('User cancelled document picker');
        } else if (res.error) {
          console.warn('ImagePicker Error: ', res.error);
        } else {

          this.setState({ fileUri: res.uri });
          this.setState({ fileType: res.type });
          this.setState({ fileName: res.fileName });
          this.setState({ fileSize: res.fileSize });

          let source = { uri: res.uri };
          let formdata = new FormData();
          formdata.append("worknoteUUID", this.props.navigation.state.params.item.worknotes_uuid)
          formdata.append("employeeName", this.state.user.EMPLOYEE_NAME)
          formdata.append("employeeUUID", this.state.user.EMPLOYEE_UUID)
          formdata.append("userId", this.state.user.USERID)
          formdata.append("userUUID", this.state.user.USERUUID)
          formdata.append("worknoteText", '')
          formdata.append("countScan", '1')
          formdata.append("worknoteScan0", { uri: source.uri, name: 'fileFromMobile', type: 'multipart/form-data' })

          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };

          console.warn('formdata:', formdata);

          fetch('http://cb.ex-group.kz:8080/ex-in-uploader/service', {
            method: 'post',
            headers: {
              "Content-Type": "multipart/form-data",
              "Accept": "application/json",
              "type": "formData"
            },
            body: formdata
          }).then(response => {
            console.warn('otvet:', response)

          }).catch(err => {
            console.log(err)
          });


        }
      }
    );
  }



  renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );
  renderModalContent = () => (
    <View style={styles.modalContent}>
      {/* <Text style={{position:'absolute', flexDirection:'column', paddingBottom:90}}>Прикрепить файл</Text> */}
      <Ionicons
        style={styles.putIcon}
        name="md-images"
        size={35}
        onPress={this.selectPhoto.bind(this)}
      />
      <Ionicons
        style={styles.putIcon}
        name="md-videocam"
        size={35}
        onPress={this.selectVideo.bind(this)}
      />
      <Ionicons
        style={styles.putIcon}
        name="md-document"
        size={35}
        onPress={this.selectFile.bind(this)}
      />
    </View>
  );

  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y
    });
  };

  handleScrollTo = p => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };


  toggle() {
    let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
      finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

    this.setState({
      expanded: !this.state.expanded
    });

    this.state.animation.setValue(initialValue);
    Animated.spring(
      this.state.animation,
      {
        toValue: finalValue
      }
    ).start();
  }

  _setMaxHeight(event) {
    this.setState({
      maxHeight: event.nativeEvent.layout.height
    });
  }

  _setMinHeight(event) {
    this.setState({
      minHeight: event.nativeEvent.layout.height
    });
  }

  async componentDidMount() {
    await AsyncStorage.getItem('user').then(value => {
      var parsed = JSON.parse(value);
      this.setState({ user: parsed });
      // console.warn('JSON:', this.state.user);
    })
      .catch((error) => {
        console.error(error);
      });
    await this.makeRemoteDetailRequest();
    await this.makeRemoteNoteRequest();
    this.deleteNotification();
  };

  makeRemoteDetailRequest  = async () => {
    const url = 'http://cb.ex-group.kz:8080/EX-IN-SERVLET/service';
    if (this.props.navigation.state.params.notif == undefined) {
      fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'USER_ID': this.state.user.USERID,
          'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
          'INSTANCE_UUID': this.props.navigation.state.params.item.worknotes_uuid,
          'ACTION': 'getWorknote',
        })
      }).then(res => res.json())
        .then(res => {
          x = [];
          res.DATA.recipients.map((object, index) => {
            x.push((index ? ', ' + '\n' + '\t' : '  ') + object.recipients_recipient_name);
            this.setState({ recipients: x });
          });
          a = res.DATA.worknote_data;
          this.setState({
            note: a
          });
        })
        .catch(error => {
          this.setState({ error, loading: false });
        });
    }
    else {
      fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'USER_ID': this.state.user.USERID,
          'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
          'INSTANCE_UUID': this.props.navigation.state.params.notif.notification.instance_uuid,
          'ACTION': 'getWorknote',
        })
      }).then(res => res.json())
        .then(res => {
          x = [];
          res.DATA.recipients.map((object, index) => {
            x.push((index ? ', ' + '\n' + '\t' : '  ') + object.recipients_recipient_name);
            this.setState({ recipients: x });
          });
          a = res.DATA.worknote_data;
          this.setState({
            note: a
          });
        })
        .catch(error => {
          this.setState({ error, loading: false });
        });
    }
  };

  makeRemoteNoteRequest = async () => {
    const url = 'http://cb.ex-group.kz:8080/ExclusiveServlet/service';
    if (this.props.navigation.state.params.notif == undefined) {
      fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'USER_ID': this.state.user.USERID,
          'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
          'INSTANCE_UUID': this.props.navigation.state.params.item.worknotes_uuid,
          'ACTION': 'getWorknoteNotes',
        })
      }).then(res => res.json())
        .then(res => {
          this.setState({ worknote: res });
          // console.warn('kisu:', this.state.worknote);
        })
        .catch(error => {
          this.setState({ error, loading: false });
        });
    }
    else {
      fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'USER_ID': this.state.user.USERID,
          'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
          'INSTANCE_UUID': this.props.navigation.state.params.notif.notification.instance_uuid,
          'ACTION': 'getWorknoteNotes',
        })
      }).then(res => res.json())
        .then(res => {
          this.setState({ worknote: res });
          // console.warn('kisu:', this.state.worknote);
        })
        .catch(error => {
          this.setState({ error, loading: false });
        });
    }
  };

  deleteNotification = () => {
    const url = 'http://cb.ex-group.kz:8080/EX-IN-SERVLET/service';

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'USER_ID': this.state.user.USERID,
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'USER_UUID': this.state.user.USERUUID,
        'INSTANCE_UUID': this.props.navigation.state.params.item.worknotes_uuid,
        'ACTION': 'deleteNotification',
      })
    }).then(res => res.json())
      .then(res => {
        // console.warn('delete:', res);
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  }


  _handlePress() {
    console.warn('Добавить файл');
  }




  render() {
    let icon = this.icons['down'];

    if (this.state.expanded) {
      icon = this.icons['up'];   //Step 4
    }
    var BContent = <Button onPress={() => this.setState({ isOpen: false })} style={[styles.btn, styles.btnModal]}>X</Button>;
    return (
      <View style={{ flex: 1 }}>
      
         <Animated.View style={[styles.container, { height: this.state.animation }]}>
            <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>


              <TouchableHighlight
                style={styles.button}
                onPress={this.toggle.bind(this)}
                underlayColor="#f1f1f1">
                <Image
                  style={styles.buttonImage}
                  source={icon}
                ></Image>
              </TouchableHighlight>
            </View>

            <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
              <Text style={{ fontWeight: 'bold' }}>№: {this.state.note.worknotes_docnumber}, Дата: {this.state.note.worknotes_docdate}</Text>
              <Text style={{ fontWeight: 'bold' }}>Тема: {this.state.note.worknotes_subject}</Text>
              <Text style={{ fontWeight: 'bold' }}>Отправитель: {this.state.note.employees_sender_name}</Text>
              <Text style={{ fontWeight: 'bold' }}>Получатели: </Text>
              <Text>{this.state.recipients}</Text>
              <Text style={{ fontWeight: 'bold' }}>Описание: </Text>
              <Text>{this.state.note.worknotes_description}</Text>
            </View>

          </Animated.View>
        <ScrollView>
          <FlatList style={styles.list}
            data={this.state.worknote.DATA}
            keyExtractor={(item) => {
              return item.notes_id;
            }}
            renderItem={(message) => {
              // console.warn('asd:', this.state.worknote);
              const item = message.item;
              let inMessage = item.notes_author_uuid === this.state.user.EMPLOYEE_UUID;
              let itemStyle = inMessage ? styles.itemOut : styles.itemIn;
              // console.warn('item:',item.notes_file);
              return (

                <View style={[styles.item, itemStyle]}>

                  <View style={[styles.balloon]}>
                    <Text style={{ color: '#F0B800' }}>{item.employees_author_name}: </Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{item.notes_text}</Text>
                    <Text style={{ fontSize: 12, alignSelf: "flex-end" }}>{item.notes_notetime}</Text>
                    {/* <Text style={{ fontWeight: 'bold' }}>Прикрепленный файл: <Text style={{ textDecorationLine: 'underline' }} onPress={() => Linking.openURL('http://cb.ex-group.kz/wnscans/53_10841439-6462-48ca-b5f0-4afe1eb48270_01_02_2019_16_20_09_fileFromMobile')} >{item.notes_file}</Text></Text> */}

                  </View>
                </View>
              )
            }}
          />

        </ScrollView>
        <View style={styles.footer}>

          <Modal
            isVisible={this.state.visibleModal === 6}
            style={styles.bottomModal}
            onBackdropPress={() => this.setState({ visibleModal: null })}
          >
            {this.renderModalContent()}
          </Modal>

          <View style={styles.inputContainer}>
            <TextInput
              ref={input => { this.textInput = input }}
              style={styles.inputs}
              placeholder="Ваше соообщение..."
              underlineColorAndroid='transparent'
              onChangeText={(text) => this.setState({ message: text })}
              clearButtonMode='always'
              value={this.state.text}
              autoCorrect={false}
            />
            <Ionicons
              style={styles.putIcon}
              name="md-attach"
              size={25}
              color="gray"
              onPress={() => this.setState({ visibleModal: 6 })}
            />
          </View>

          <TouchableOpacity style={styles.btnSend} onPress={() => this.sendMessage()}>
            <Image source={{ uri: "https://png.icons8.com/small/75/ffffff/filled-sent.png" }} style={styles.iconSend} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

NoteDetail.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  container: {
    backgroundColor: '#fff',
    margin: 10,
    overflow: 'hidden',
    borderRadius: 5,
  },
  titleContainer: {
    flexDirection: 'row',
  },
  title: {
    flex: 1,
    padding: 10,
    color: '#2a2f43',
    fontWeight: 'bold'
  },
  button: {

  },
  buttonImage: {
    width: 30,
    height: 25
  },
  body: {
    padding: 5,
    paddingTop: 0
  },
  list: {
    paddingHorizontal: 17,
  },
  footer: {
    flexDirection: 'row',
    height: 60,
    backgroundColor: '#eeeeee',
    paddingHorizontal: 10,
    padding: 5,

  },
  btnSend: {
    backgroundColor: "#00BFFF",
    width: 40,
    height: 40,
    borderRadius: 360,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconSend: {
    width: 30,
    height: 30,
    alignSelf: 'center',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    marginRight: 10,
    justifyContent: 'center',

  },
  inputs: {
    height: 40,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,

  },
  putIcon: {
    padding: 30,
  },
  balloon: {
    maxWidth: 300,
    padding: 5,
    borderRadius: 20,
  },
  itemIn: {
    alignSelf: 'flex-start'
  },
  
  itemOut: {
    alignSelf: 'flex-end'
  },
  item: {
    marginVertical: 5,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 5,
    alignSelf: 'flex-start'
  },
  modalContent: {
    flexDirection: 'row',
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },
};

export default NoteDetail;


