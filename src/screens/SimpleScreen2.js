import React, { Component } from 'react';
import { SafeAreaView, Text } from 'react-native';
import PropTypes from 'prop-types';

import { Button } from '../components/common/Button';

class SimpleScreen2 extends Component {
  static navigationOptions = {
    title: 'Мои задачи',
  };

  render() {
    return (
      <SafeAreaView style={styles.containerStyle}>
        <Text>Страница на разработке</Text>
        
      </SafeAreaView>
    );
  }
}

SimpleScreen2.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default SimpleScreen2;
