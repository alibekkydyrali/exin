import React, { Component } from 'react';
import { View, SafeAreaView, TouchableOpacity, YellowBox, AsyncStorage, RefreshControl, Platform } from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Left, Right, Icon, Badge, Row, } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button } from '../components/common/Button';
import FCM, { NotificationActionType } from "react-native-fcm";
import { registerKilledListener, registerAppListener } from "../actions/pushnotifications";
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
import { navigateTo } from '../actions';
registerKilledListener();

class NoteScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      user: '',
      count: '',
      data: '',
      refreshing: false,
      id: '',
      token: "",
      tokenCopyFeedback: ""
    };
  }


  notificationRequest = () => {
    this.setState({ loading: true });
    fetch('http://cb.ex-group.kz:8080/EX-IN-SERVLET/service', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'USER_ID': this.state.user.USERID,
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'USER_UUID': this.state.user.USERUUID,
        'ACTION': 'getNotificationsByUser',
      })
    }).then(res => res.json())
      .then(res => {
        this.setState({ data: res });
        // console.warn('getnotification:', { data: this.state.data.data});
        b = [];
        if (res.data != 0) {
          res.data.map((object) => {
            b.push(object.object_name);
            this.setState({ count: b })
          });

        }

        this.setState({
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  SendId = () => {
    FCM.requestPermissions();
    fetch('http://cb.ex-group.kz:8080/EX-IN-SERVLET/service', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'USERNAME': this.state.user.USERNAME,
        'ID': this.state.id,
        'ACTION': 'regFireBaseId'

      }),
    }).then((response) => {
      // console.warn(response);
    })

      .catch((error) => {
        console.error(error);
      });
  }

  async componentDidMount() {
    FCM.createNotificationChannel({
      id: 'default',
      name: 'Default',
      description: 'used for example',
      priority: 'high'
    })
    registerAppListener(this.props.navigation);
    FCM.getInitialNotification().then(notif => {
      this.setState({
        initNotif: notif
      });
      // console.warn('asd:', notif);
      if (notif && notif.objectType === 'worknotes') {
          this.props.navigation.navigate('Note', { notif });
      }
    });
    try {
      let result = await FCM.requestPermissions({
        badge: false,
        sound: true,
        alert: true
      });
    } catch (e) {
      console.error(e);
    }

    FCM.getFCMToken().then(token => {
      console.log("TOKEN (getFCMToken)", token);
      this.setState({ token: token || "" });
    });

    if (Platform.OS === "ios") {
      FCM.getAPNSToken().then(token => {
        console.log("APNS TOKEN (getFCMToken)", token);
      });
    }

    // topic example
    // FCM.subscribeToTopic('sometopic')
    // FCM.unsubscribeFromTopic('sometopic')

    this.props.navigation.setParams({
      onNavigateTo: this.onNavigateTo.bind(this),
    });
    await AsyncStorage.getItem('user').then(value => {
      var parsed = JSON.parse(value);
      this.setState({ user: parsed });
      // console.warn('JSON:', parsed);
    })
    await AsyncStorage.getItem('id').then(value2 => {
      var parsed2 = JSON.parse(value2);
      this.setState({ id: parsed2 });
      // console.warn('id:', parsed2);
    })
      .catch((error) => {
        console.error(error);
      });
    this.notificationRequest();


    this.SendId();
  }




  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => {
        this.notificationRequest();
      }
    );
  };



  onNavigateTo(path, params) {
    this.props.navigateTo(path, params);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Список',
      headerRight: (
        <View style={{ marginRight: 20 }}>
          <Ionicons
            name={'md-exit'}
            size={25}
            color={'white'}
            onPress={() => {
              if (navigation.state.params) {
                const params = navigation.state.params;
                if (params.onNavigateTo) {
                  const { onNavigateTo } = params;
                  return onNavigateTo('LogOut');
                }
              }
            }}
          />
        </View>
      ),
      headerLeft: (
        <View style={{ marginLeft: 20 }}>
          <Ionicons
            name={'ios-list'}
            size={35}
            color={'white'}
            onPress={() => {
              if (navigation.state.params) {
                const params = navigation.state.params;
                if (params.onNavigateTo) {
                  const { onNavigateTo } = params;
                  return onNavigateTo('DrawerToggle');
                }
              }
            }}
          />
        </View>
      ),
    };
  };

  render() {
    return (
      <Container >
        <Content
          refreshControl={
            <RefreshControl
              onRefresh={this.handleRefresh}
              refreshing={this.state.refreshing}
            />
          }>
          <List>
            <ListItem
              title={'Страница на разработке'}
              onPress={() => { this.props.navigation.navigate('Simple2'); }}
            >
              <Left>
                <Text>Мои задачи</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem
              title={'Страница на разработке'}
              onPress={() => { this.props.navigation.navigate('Simple3'); }}
              badge={{value:3}}
            >
              <Left>
                <Text>Задачи на контроле</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem
              title={'Служебные записки'}
              onPress={() => { this.props.navigation.navigate('Simple4'); }}
            >
              <Left>
                <Text>Служебные записки</Text>
              </Left>
              <Right>
                <Badge warning >
                  <Text>{this.state.count.length}</Text>
                </Badge>
              </Right>
            </ListItem>
            {/* <ListItem
              title={'Служебные записки Отправленные'}
              onPress={() => { this.props.navigation.navigate('OfficeNotes1'); }}
            >
              <Left>
                <Text>Отправленные служебные записки</Text>
              </Left>
              <Right>
                <Badge warning >
                  <Text>{this.state.count.length}</Text>
                </Badge>
              </Right>
            </ListItem>
            <ListItem
              title={'Служебные записки Принятые'}
              onPress={() => { this.props.navigation.navigate('OfficeNotes2'); }}
            >
              <Left>
                <Text>Принятые служебные записки</Text>
              </Left>
              <Right>
                <Badge warning >
                  <Text>{this.state.count.length}</Text>
                </Badge>
              </Right>
            </ListItem> */}
            <ListItem
              title={'Страница на разработке'}
              onPress={() => { this.props.navigation.navigate('Simple5'); }}
            >
              <Left>
                <Text>Новости</Text>
              </Left>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>



        </Content>
      </Container>
    );
  }
}

NoteScreen.propTypes = {
  navigation: PropTypes.object,
  navigateTo: PropTypes.func,
};

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
};
export default connect(null, { navigateTo })(NoteScreen);
