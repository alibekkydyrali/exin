import { StackNavigator } from 'react-navigation';

import NoteTab from './NoteTabNavigator';
import NoteScreen from '../screens/NoteScreen';
import LogoutScreen from '../screens/LogoutScreen';

const routeConfig = {
  Home: NoteScreen,
  LogOut: LogoutScreen,
};

const navigationConfig = {
  initialRouteName: 'Home',
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#004d40',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  },
};

const NoteStackNavigator = StackNavigator(routeConfig, navigationConfig);

export default NoteStackNavigator;
