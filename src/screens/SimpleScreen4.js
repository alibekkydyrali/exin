import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator, AsyncStorage, Dimensions, Button, TouchableOpacity } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Modal from "react-native-modal";
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
import ActionButton from 'react-native-action-button';

class SimpleScreen4 extends Component {

  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      title: 'Служебные записки',
      headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.navigate('NoteScreen')}
        >
          <Ionicons
            name={'md-arrow-back'}
            size={25}
            color={'white'}
            style={{ marginLeft: 15 }}
          />
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity
          onPress={() => navigation.state.params.func1()}
        >
          <Ionicons
            name={'md-options'}
            size={25}
            color={'white'}
            style={{ marginRight: 15 }}
          />
        </TouchableOpacity>
      ),
    };
  };
  func2() {
    this.setState({ visibleModal: 2 })
  }
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      datarec: [],
      datasend: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      user: '',
      object_name: '',
      status: false,
      notification: '',
      visibleModal: null,
    };
    this.func2 = this.func2.bind(this)
    this.props.navigation.setParams({
      func1: this.func2
    })
  }

  async componentDidMount() {

    await AsyncStorage.getItem('user').then(value => {
      var parsed = JSON.parse(value);
      this.setState({ user: parsed });
      // console.warn('JSON:', parsed);
    })
      .catch((error) => {
        console.error(error);
      });
    this.makeRemoteRequestRecipient();
    this.makeRemoteRequestSender();
    this.notificationRequest();
  };



  makeRemoteRequestSender = () => {
    const url = 'http://cb.ex-group.kz:8080/EX-IN-SERVLET/service';
    this.setState({ loading: true });

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'DATATYPE': 'list',
        'ACTION': 'getWorknotes',
        'STATUS': 'sender',
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'FILTER': {
          'count': '1',
          '1': {
            'preoperator': 'AND',
            'field': 'docm1068_closed',
            'predicate': '=',
            'value': 'false',
            'postoperator': ''
          }
        },
      })
    }).then(res => res.json())
      .then(res => {
        this.setState({
          datasend: res,
          error: res.error || null,
          loading: false,
          refreshing: false
        });
        //  console.warn('otvet:',res.data);
        // AsyncStorage.setItem('note', JSON.stringify(res));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  makeRemoteRequestSenderClosed = () => {
    const url = 'http://cb.ex-group.kz:8080/EX-IN-SERVLET/service';
    this.setState({ loading: true });

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'DATATYPE': 'list',
        'ACTION': 'getWorknotes',
        'STATUS': 'sender',
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'FILTER': {
          'count': '1',
          '1': {
            'preoperator': 'AND',
            'field': 'docm1068_closed',
            'predicate': '=',
            'value': 'true',
            'postoperator': ''
          }
        },
      })
    }).then(res => res.json())
      .then(res => {
        this.setState({
          datasend: res,
          visibleModal: null,
          error: res.error || null,
          loading: false,
          refreshing: false,
        });
        //  console.warn('otvet:',res.data);
        // AsyncStorage.setItem('note', JSON.stringify(res));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  makeRemoteRequestRecipient = () => {
    const url = 'http://cb.ex-group.kz:8080/EX-IN-SERVLET/service';
    this.setState({ loading: true });

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'DATATYPE': 'list',
        'ACTION': 'getWorknotes',
        'STATUS': 'recipient',
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'FILTER': {
          'count': '1',
          '1': {
            'preoperator': 'AND',
            'field': 'docm1068_closed',
            'predicate': '=',
            'value': 'false',
            'postoperator': ''
          }
        },
      })
    }).then(res => res.json())
      .then(res => {
        this.setState({
          datarec: res,
          error: res.error || null,
          loading: false,
          refreshing: false
        });
        //  console.warn('otvet:',res.data);
        // AsyncStorage.setItem('note', JSON.stringify(res));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  makeRemoteRequestRecipientClosed = () => {
    const url = 'http://cb.ex-group.kz:8080/EX-IN-SERVLET/service';
    this.setState({ loading: true });

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'DATATYPE': 'list',
        'ACTION': 'getWorknotes',
        'STATUS': 'recipient',
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'FILTER': {
          'count': '1',
          '1': {
            'preoperator': 'AND',
            'field': 'docm1068_closed',
            'predicate': '=',
            'value': 'true',
            'postoperator': ''
          }
        },
      })
    }).then(res => res.json())
      .then(res => {
        this.setState({
          datarec: res,
          error: res.error || null,
          loading: false,
          refreshing: false,
          visibleModal: null
        });
        //  console.warn('otvet:',res.data);
        // AsyncStorage.setItem('note', JSON.stringify(res));
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  notificationRequest = () => {
    this.setState({ loading: true });
    fetch('http://cb.ex-group.kz:8080/EX-IN-SERVLET/service', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'USER_ID': this.state.user.USERID,
        'EMPLOYEE_UUID': this.state.user.EMPLOYEE_UUID,
        'USER_UUID': this.state.user.USERUUID,
        'ACTION': 'getNotificationsByUser',
      })
    }).then(res => res.json())
      .then(res => {
        // console.warn(res);
        this.setState({
          notification: res.data,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleRefreshSend = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequestSender();
        this.notificationRequest();
      }
    );
  };

  handleRefreshRec = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequestRecipient();
        this.notificationRequest();
      }
    );
  };


  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar placeholder="Поиск..." lightTheme round />;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  _renderTitleIndicator() {
    return <PagerTitleIndicator
      titles={['Принятые', 'Отправленные']}
      itemStyle={{ width: Dimensions.get('window').width / 2 }}
      selectedItemStyle={{ width: Dimensions.get('window').width / 2 }}
      selectedItemTextStyle={{ color: '#004d40' }}
      selectedBorderStyle={{ backgroundColor: '#004d40' }}
      itemTextStyle={{ color: '#000' }}
      trackScroll={true}
    />;
  }

  renderModalContent = () => (
    <View style={styles.modalContent}>
      <Text>Фильтры</Text>

      <TouchableOpacity onPress={() => { this.makeRemoteRequestRecipientClosed() }}
        style={{
          borderRadius: 5,
          backgroundColor: 'rgba(231,76,60,1)',
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          width: 180,
          marginBottom: 10
        }}
      >
        <Text>Принятые закрытые</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => { this.makeRemoteRequestSenderClosed() }}
        style={{
          borderRadius: 5,
          backgroundColor: 'rgba(231,76,60,1)',
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          width: 180
        }}
      >
        <Text>Отправленные закрытые</Text>
      </TouchableOpacity>

    </View>
  );

  render() {

    return (
      

      <IndicatorViewPager
        style={{ flex: 1, backgroundColor: 'white', flexDirection: 'column-reverse', }}
        indicator={this._renderTitleIndicator()}
      >

        <View style={{ backgroundColor: '#e5e5e5' }}>
        
          <Modal
            isVisible={this.state.visibleModal === 2}
            animationIn="slideInRight"
            animationOut="slideOutLeft"
            onBackdropPress={() => this.setState({ visibleModal: null })}>
            {this.renderModalContent()}
          </Modal>
          
          <FlatList
            data={this.state.datarec.data}
            keyExtractor={item => item.worknotes_id}
            ItemSeparatorComponent={this.renderSeparator}
            // ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            onRefresh={this.handleRefreshRec}
            refreshing={this.state.refreshing}
            renderItem={(message) => {
              const item = message.item;
              const a = [];

              if (this.state.notification != 0) {
                  this.state.notification.map((object) => {
                    a.push(object.instance_uuid);
                  });
              }
              let read = a.includes(item.worknotes_uuid);
              let itemStyle = read ? styles.read : styles.unread;
              if (item.closed_bool == 't') {
                var badge = { containerStyle: { backgroundColor: 'red', width: 1, height: 10 } };
              }
              return (
                <ListItem
                  title={'№: ' + item.worknotes_docnumber + ', ' + 'Дата: ' + item.worknotes_docdate}
                  subtitle={item.worknotes_subject}
                  subtitleNumberOfLines={2}
                  containerStyle={itemStyle}
                  onPress={() => { this.props.navigation.navigate('Note', { item }) }}
                  badge={badge}
                />
              )
            }}
          />
          <ActionButton buttonColor="rgba(231,76,60,1)"  onPress={() => this.props.navigation.navigate('CreateNote')}>
        </ActionButton>
        </View>

        <View style={{ backgroundColor: '#e5e5e5' }}>
          <FlatList
            data={this.state.datasend.data}
            keyExtractor={item => item.worknotes_id}
            ItemSeparatorComponent={this.renderSeparator}
            // ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            onRefresh={this.handleRefreshSend}
            refreshing={this.state.refreshing}
            renderItem={(message) => {
              const item = message.item;
              const a = [];

              if (this.state.notification != 0) {
                this.state.notification.map((object) => {
                  a.push(object.instance_uuid);
                });
              }
              let read = a.includes(item.worknotes_uuid);
              let itemStyle = read ? styles.read : styles.unread;
              if (item.closed_bool == 't') {
                var badge = { containerStyle: { backgroundColor: 'red', width: 1, height: 10 } };
              }

              return (
                <ListItem
                  title={'№: ' + item.worknotes_docnumber + ', ' + 'Дата: ' + item.worknotes_docdate}
                  subtitle={item.worknotes_subject}
                  subtitleNumberOfLines={2}
                  containerStyle={itemStyle}
                  onPress={() => { this.props.navigation.navigate('Note', { item }) }}
                  badge={badge}
                />
              )
            }}
          />
          <ActionButton buttonColor="rgba(231,76,60,1)" onPress={() => this.props.navigation.navigate('CreateNote')}>
        </ActionButton>
        </View>
      </IndicatorViewPager>



    );
  }


}

SimpleScreen4.propTypes = {
  navigation: PropTypes.object,
};

const styles = {
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  read: {
    backgroundColor: 'pink',
    borderBottomWidth: 0,
  },
  unread: {
    backgroundColor: 'white',
    borderBottomWidth: 0,
  },
  modalContent: {
    flexDirection: 'column',
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
    height: 150,
    marginBottom: 300,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
};



export default SimpleScreen4;
